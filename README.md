# PIZZERIA

---

## Spis treści
* [Informacje o projekcie](#informacje-o-projekcie)
* [Instrukcja](#instrukcja)
* [Narzędzia](#narzędzia)
* [Funkcjonalności](#funkcjonalności)
* [Podział pracy](#podział-pracy)

---

## Informacje o projekcie

### Opis projektu:

Aplikacja służy do zamawiania posiłków.

System obsługuje jedną z popularnych pizzeri i umożliwia zamówienie pizz oraz napoi.

### Aplikacja umożliwia:

1)Zamówienie produktów w zestawach

2)Zamównie pojedynczych produktów

3)Zamównienie produktów w promocjach:

 -pizza 50/50
 
 -przy zakupie 2 pizz napój gratis
 
 -rabat 20% przy zakupach powyżej 100 zł
 
4)Generowanie paragonu po zakończeniu zamówienia

---

## Instrukcja

### Ważna informacja !!!

Pliki tekstowe należy przenieść do folderu "build"

Program należy otworzyć w pełnoekranowym oknie konsoli.

Program działa na prostej zasadzie:

Po uruchomieniu programu przed użytkownikiem pojawi się kolorowe menu pizzeri wczytane z pliku .txt.

Pod menu powinien wyświetlić się główny interfejs programu, od którego użytkownik zacznie zamawianie posiłku.
```
[1] Pizza
[2] Pizza pół na pół
[3] Zestawy
[4] Napoje zimne
[5] Napoje cieple



Twój wybór (wpisz numer): 
```
 
Aby wybrać jedną z opcji użytkownik powinien wczytać do programu odpowiadający jej numer.
Po wybraniu opcji z głównego interfejsu, będzie można wybrać rodzaj pizzy bądź napoju, a następnie 
wielkość oraz ilość. Wybór będzie wyglądał identycznie jak w przypadku głównego interfejsu.

Po wybraniu produktu przez użytkownika, wyświetli się zamówienie oraz jego koszt, a także pytanie czy użytkownik chciałby zamówić coś jeszcze.
```
               Twoje zamówienie
              1 x Pizza Margarita

Do zapłaty:  40zl

Chcesz zamówić cos jeszcze? T / N:
```

Aby przejść dalej, należy wprowadzić do programu T(tak) lub N(nie).
W przypadku wybrania opcji "tak", użytkownik zostanie przeniesiony do głównego interfejsu, gdzie będzie mógł dodać do swojego zamówienia kolejne pozycje z menu.
Natomiast po wybrania opcji "nie", zostanie wygenerowany paragon z listą wybranych produktów oraz kwotą należną za złożone zamówienie.

### Promocje!!!

1)Jeśli użytkownik zamówi więcej niż 1 pizze, zostanie przeniesiony do okienka z możliwością wybrania darmowego napoju.

2)W przypadku, gdy wartość zamówienie wyniesie ponad 100 zł, wartość końcowa zamówienia zostanie obniżona o 20%.

Prosty w użyciu program, nie powinien sprawiać użytkownikom kłopotów.

W razie problemów z aplikacją, należy skontaktować się z twórcami programu.

---

## Narzędzia
Qt Creator 5.0.0-rc1 (Community)

### Biblioteki:

<iostream>

<vector>

<string>

<windows.h>

<fstream>

---

## Funkcjonalności

feature-Data - Zawiera deklaracje i nazwy produktów

feature-Order - Zawiera funkcje służące do zamawiania

feature-readMenu - Zawiera kod wczytujący Menu

feature_printMenu -  Zawiera kod drukujący Menu

feature-readReceipt - Zawiera kod wczytujący Paragon

feature-printReceipt - Zawiera kod drukujący Paragon 

---

## Podział pracy

**Magdalena Kłósek:**

1) Pizza 

2) Klasy

3) Napoje

4) Warunki dla pizzy & napoi

5) Pliki tekstowe

6) Licznik promocji 2x pizza + napój gratis



**Konrad Wszołek:**

1) Pizze 50/50

2) Zestawy

3) Warunki dla pizze 50/50 i zestawów

4) Promocja 20%

5) Zapis danych na paragonie

6) Zabezpieczenia













