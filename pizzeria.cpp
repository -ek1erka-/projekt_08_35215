#include "pizzeria.h"

//================================================

cPizzeria::cPizzeria()
{
    vMenu.reserve(10);
    readMenu();
    vReceipt.reserve(10);
}

//================================================

void cPizzeria::readMenu()
{
    ifstream file; // ifstream only reading
    string line;

    file.open("Menu.txt", ios::out);
    if (!file.good())
    {
        file.close();
        cout << "Problem z plikiem\n";
        return;
    }

    while (getline(file, line))
    {
        vMenu.push_back(line);
    }

    file.close();
}

//================================================

void cPizzeria::printMenu()
{
    int flag = 0;

    for (const auto& line : vMenu)
    {
        if (flag < 20)
            colorTxt(line, 0x0C);
        else if(flag > 20 && flag < 200)
            colorTxt(line, 0x0E);
        else
            colorTxt(line, 0x0F);
        cout << '\n';
        flag++;
    }

}

//================================================

void cPizzeria::colorTxt(string Txt, int color)
{
    HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(h, color);
    cout << Txt;
    SetConsoleTextAttribute(h, 0x0F); // Reset color to white
}


//================================================

void cPizzeria::readReceipt()
{
    ifstream file; // ifstream only reading
    string line;

    file.open("Rachunek.txt");
    if (!file.good())
    {
        file.close();
        cout << "Problem z plikiem\n";
        return;
    }

    while (getline(file, line))
    {
        vReceipt.push_back(line);
    }

    file.close();
}

//================================================

void cPizzeria::printReceipt()
{
    int flag = 0;

    for (const auto& line : vReceipt)
    {
        cout << line;
        cout << '\n';
        flag++;
    }

}

//================================================

cData::cData()
{
//Pizzas

        //Red pizzas

    piz1="Pizza Margarita",
    piz2="Pizza Farmerska",
    piz3="Pizza Calabrese",
    piz4="Pizza Prosciutto e funghi",

        //White pizzas

    piz5="Pizza Hawajska",
    piz6="Pizza Quattro Formagia",
    piz7="Pizza Salmone",
    piz8="Pizza Grzybiarza";

// 50/50 pizzas

        //Red pizzas

    piz50_50_1="Margarita/Farmerska",
    piz50_50_2="Margarita/Calabrese",
    piz50_50_3="Margarira/Prosciutto e funghi",
    piz50_50_4="Prosciutto e funghi/Farmerska",
    piz50_50_5="Prosciutto e funghi/Calabrese",
    piz50_50_6="Calabrese/Farmerska",

        //White pizzas

    piz50_50_7="Hawajska/Quattro Formagia",
    piz50_50_8="Hawajska/Salmone",
    piz50_50_9="Hawajska/Grzybiarza",
    piz50_50_10="Quattro Formagia/Salmone",
    piz50_50_11="Quattro Formagia/Grzybiarza",
    piz50_50_12="Salmone/Grzybiarza";

//Sets

    set_1="Zestaw studencki",
    set_2="Zestaw dla dzieci",
    set_3="Zestaw impresowy",
    set_4="Zestaw wakacyjny";

//Drinks

        //Cold

               //Juices

    drink_cold_orange="Sok Pomaranczowy",
    drink_cold_apple="Sok jablkowy",
    drink_cold_cherry="Sok wisniowy",
    drink_cold_mintapple="Sok jablko-mieta",
    drink_cold_applepeach="Sok jablko-brzoskwinia",

               //Rest

    drink_cold_cola="Coca Cola",
    drink_cold_pepsi="Pepsi",
    drink_cold_fanta="Fanta",
    drink_cold_nesteacitrus="Nestea cytrus",
    drink_cold_nesteapeach="Nestea brzoskwinia",
    drink_cold_water="Woda mineralna",

        //Hot

               //Coffees

    drink_hot_latte="Latte Macchiato",
    drink_hot_cappucino="Cappucino",
    drink_hot_espresso="Espresso",

               //Teas

    drink_hot_blacktea="Herbata czarna",
    drink_hot_greentea="Hertaba zielona";
}

//================================================

cOrder::cOrder()
{
                            //Receipt - open file
    fstream file;

    file.open("Rachunek.txt", ios::out);
    if(file.good() != true)
    {
        file.close();
        cout << "Problem z plikiem\n";
    }
    file << "\t    PARAGON FISKALNY: \n\n";
    file<<"    --------Twoje zamowienie--------\n";

starting:                           //Start order

    printMenu();

    cout<<"[1] Pizza\n";
    cout<<"[2] Pizza pol na pol\n";
    cout<<"[3] Zestawy\n";
    cout<<"[4] Napoje zimne\n";
    cout<<"[5] Napoje cieple\n\n";
    cout<<"\nTwoj wybor (wpisz numer): ";
    cin>>option;                                  //Select order option


    if(option==1)                                 //Choose pizza / pizzas
    {
        cout<<"\n1) "<<piz1<<"\n\n";
        cout<<"2) "<<piz2<<"\n\n";
        cout<<"3) "<<piz3<<"\n\n";
        cout<<"4) "<<piz4<<"\n\n";
        cout<<"5) "<<piz5<<"\n\n";
        cout<<"6) "<<piz6<<"\n\n";
        cout<<"7) "<<piz7<<"\n\n";
        cout<<"8) "<<piz8<<"\n\n";
        cout<<"0) Powrot do MENU";
        cout<<"\n\nTwoj wybor:";
        cin>>pizzaoption;
        if(pizzaoption>=1 && pizzaoption<=8)
        {
            cout<<"\n1) Mala (32 cm) \n"<<"2) Srednia (45 cm) \n"<<"3) Duza (60 cm)\n";
            cout<<"\n\nWybierz rozmiar:";                                   //Choose size
            cin>>pizzaoption1;
            if(pizzaoption1>=1 && pizzaoption1<=3)
            {
                cout<<"\nPodaj ilosc: ";
            }//Enter quantity
            else
            {
                system("cls");
                cout<<"Podano niewlasciwa wielkosc, Sprobuj ponownie\n";
                system("PAUSE");
                goto starting;
            }
            cin>>qty;
            count=count+qty;
            switch(pizzaoption1)
            {
            case 1: //Small
                if( pizzaoption == 2 || pizzaoption == 3  || pizzaoption == 6  || pizzaoption == 8 )
                {
                    option = 35*qty;
                }
                else if( pizzaoption == 1)
                {
                    option = 30*qty;
                }
                else if( pizzaoption == 4 || pizzaoption == 5 )
                {
                    option = 32*qty;
                }
                else if( pizzaoption == 7 )
                {
                    option = 37*qty;
                }
                break;

            case 2: //Medium
                if( pizzaoption == 2 || pizzaoption == 3  || pizzaoption == 6  || pizzaoption == 8 )
                {
                    option = 45*qty;
                }
                else if( pizzaoption == 1)
                {
                    option = 40*qty;
                }
                else if( pizzaoption == 4 || pizzaoption == 5 )
                {
                    option = 42*qty;
                }
                else if( pizzaoption == 7 )
                {
                    option = 48*qty;
                }
                break;

            case 3: //Large
                if( pizzaoption == 2 || pizzaoption == 3  || pizzaoption == 6  || pizzaoption == 8 )
                {
                    option = 60*qty;
                }
                else if( pizzaoption == 1)
                {
                    option = 55*qty;
                }
                else if( pizzaoption == 4 || pizzaoption == 5 )
                {
                    option = 56*qty;
                }
                else if( pizzaoption == 7 )
                {
                    option = 65*qty;
                }
                break;

            }
        }
        else if(pizzaoption == 0)
        {
            system("cls");
            goto starting;
        }
        else
        {
            system("cls");
            cout<<"Wprowadzono niedozwolony znak, sprobuj ponownie\n";
            system("PAUSE");
            goto starting;
        }

        system("cls");
        switch (pizzaoption)                               //Choosen options display
        {
        case 1:
            cout<<"\t\t--------Twoje zamowienie---------\n";
            cout<<"\t\t      "<<qty<<" x "<<piz1;
            cout<<"\n\nDo zaplaty:  "<<option<<"zl";
            file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
            file<<"\t   "<<piz1;
            sum=sum+option;
            break;
        case 2:
            cout<<"\t\t--------Twoje zamowienie---------\n";
            cout<<"\t\t      "<<qty<<" x "<<piz2;
            cout<<"\n\nDo zaplaty:  "<<option<<"zl";
            file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
            file<<"\t   "<<piz2;
            sum=sum+option;
            break;
        case 3:
            cout<<"\t\t--------Twoje zamowienie---------\n";
            cout<<"\t\t      "<<qty<<" x "<<piz3;
            cout<<"\n\nDo zaplaty:  "<<option<<"zl";
            file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
            file<<"\t   "<<piz3;
            sum=sum+option;
            break;
        case 4:
            cout<<"\t\t--------Twoje zamowienie---------\n";
            cout<<"\t\t      "<<qty<<" x "<<piz4;
            cout<<"\n\nDo zaplaty:  "<<option<<"zl";
            file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
            file<<"\t   "<<piz4;
            sum=sum+option;
            break;
        case 5:
            cout<<"\t\t\t--------Twoje zamowienie---------\n";
            cout<<"\t\t      "<<qty<<" x "<<piz5;
            cout<<"\n\nDo zaplaty:  "<<option<<"zl";
            file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
            file<<"\t   "<<piz5;
            sum=sum+option;
            break;
        case 6:
            cout<<"\t\t\t--------Twoje zamowienie---------\n";
            cout<<"\t\t      "<<qty<<" x "<<piz6;
            cout<<"\n\nDo zaplaty:  "<<option<<"zl";
            file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
            file<<"\t   "<<piz6;
            sum=sum+option;
            break;
        case 7:
            cout<<"\t\t\t--------Twoje zamowienie---------\n";
            cout<<"\t\t      "<<qty<<" x "<<piz7;
            cout<<"\n\nDo zaplaty:  "<<option<<"zl";
            file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
            file<<"\t   "<<piz7;
            sum=sum+option;
            break;
        case 8:
            cout<<"\t\t\t--------Twoje zamowienie---------\n";
            cout<<"\t\t      "<<qty<<" x "<<piz8;
            cout<<"\n\nDo zaplaty:  "<<option<<"zl";
            file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
            file<<"\t   "<<piz8;
            sum=sum+option;
            break;

        }
        if(count >= 2)
        {
            cout<<"\n1) "<<drink_cold_orange<<"\n";
            cout<<"2) "<<drink_cold_apple<<"\n";
            cout<<"3) "<<drink_cold_cherry<<"\n";
            cout<<"4) "<<drink_cold_mintapple<<"\n";
            cout<<"5) "<<drink_cold_applepeach<<"\n";
            cout<<"6) "<<drink_cold_cola<<"\n";
            cout<<"7) "<<drink_cold_pepsi<<"\n";
            cout<<"8) "<<drink_cold_fanta<<"\n";
            cout<<"9) "<<drink_cold_nesteacitrus<<"\n";
            cout<<"10) "<<drink_cold_nesteapeach<<"\n";
            cout<<"11) "<<drink_cold_water<<"\n";
            cout<<"\n\nWybierz swoj darmowy napoj: ";
            cin>>pizzaoption1;
            if(pizzaoption1>=1 && pizzaoption1<=11)
            {
                switch(pizzaoption1)
                {
                case 1: option = 0.00;
                    break;

                case 2: option = 0.00;
                    break;

                case 3: option = 0.00;
                    break;

                case 4: option = 0.00;
                    break;

                case 5: option = 0.00;
                    break;

                case 6: option = 0.00;
                    break;

                case 7: option = 0.00;
                    break;

                case 8: option = 0.00;
                    break;

                case 9: option = 0.00;
                    break;

                case 10: option = 0.00;
                    break;

                case 11: option = 0.00;
                    break;
                }
                system("cls");
                switch (pizzaoption1)             //Chosen promotion cold drink / drinks
                {
                case 1:
                    cout<<"\t\t ---------Wybrany napoj----------\n";
                    cout<<"\t\t\t"<<drink_cold_orange;
                    file<<"\n    "<<count/2<<" x "<<option<<"zl";
                    file<<"\t   "<<drink_cold_orange;
                    break;
                case 2:
                    cout<<"\t\t ---------Wybrany napoj----------\n";
                    cout<<"\t\t\t"<<drink_cold_apple;
                    file<<"\n    "<<count/2<<" x "<<option<<"zl";
                    file<<"\t   "<<drink_cold_apple;
                    break;
                case 3:
                    cout<<"\t\t ---------Wybrany napoj----------\n";
                    cout<<"\t\t\t"<<drink_cold_cherry;
                    file<<"\n    "<<count/2<<" x "<<option<<"zl";
                    file<<"\t   "<<drink_cold_cherry;
                    break;
                case 4:
                    cout<<"\t\t ---------Wybrany napoj----------\n";
                    cout<<"\t\t\t"<<drink_cold_mintapple;
                    file<<"\n    "<<count/2<<" x "<<option<<"zl";
                    file<<"\t   "<<drink_cold_mintapple;
                    break;
                case 5:
                    cout<<"\t\t ---------Wybrany napoj----------\n";
                    cout<<"\t\t\t"<<drink_cold_applepeach;
                    file<<"\n    "<<count/2<<" x "<<option<<"zl";
                    file<<"\t   "<<drink_cold_applepeach;
                    break;
                case 6:
                    cout<<"\t\t ---------Wybrany napoj----------\n";
                    cout<<"\t\t\t"<<drink_cold_cola;
                    file<<"\n    "<<count/2<<" x "<<option<<"zl";
                    file<<"\t   "<<drink_cold_cola;
                    break;
                case 7:
                    cout<<"\t\t ---------Wybrany napoj----------\n";
                    cout<<"\t\t\t"<<drink_cold_pepsi;
                    file<<"\n    "<<count/2<<" x "<<option<<"zl";
                    file<<"\t   "<<drink_cold_pepsi;
                    break;
                case 8:
                    cout<<"\t\t ---------Wybrany napoj----------\n";
                    cout<<"\t\t\t"<<drink_cold_fanta;
                    file<<"\n    "<<count/2<<" x "<<option<<"zl";
                    file<<"\t   "<<drink_cold_fanta;
                    break;
                case 9:
                    cout<<"\t\t ---------Wybrany napoj----------\n";
                    cout<<"\t\t\t"<<drink_cold_nesteacitrus;
                    file<<"\n    "<<count/2<<" x "<<option<<"zl";
                    file<<"\t   "<<drink_cold_nesteacitrus;
                    break;
                case 10:
                    cout<<"\t\t ---------Wybrany napoj----------\n";
                    cout<<"\t\t\t"<<drink_cold_nesteapeach;
                    file<<"\n    "<<count/2<<" x "<<option<<"zl";
                    file<<"\t   "<<drink_cold_nesteapeach;
                    break;
                case 11:
                    cout<<"\t\t ---------Wybrany napoj----------\n";
                    cout<<"\t\t\t"<<drink_cold_water;
                    file<<"\n    "<<count/2<<" x "<<option<<"zl";
                    file<<"\t   "<<drink_cold_water;
                    break;

                }

            }
            cout<<"\n\nChcesz zamowic cos jeszcze? T / N:";
            cin>>gotobeginning;
            if(gotobeginning=='T' || gotobeginning=='t')
            {
                goto starting;
                //return 0;
            }
            else if(gotobeginning=='N' || gotobeginning=='n')
            {
                system( "cls" );
                if(sum>=100)
                {
                    sum=sum*0.8;
                }
                file<<"\n\nDo zaplaty:  "<< sum <<"\n\n";
                file<<"\nZamowienie powyzej 100 zl - 20% znizki\n";
                file<<"Kierowca dostarczy twoje zamowienie w przeciagu 40 minut";
                file<<"\nDziekujemy i zapraszamy ponownie\n";

                file.close();
                readReceipt();
                printReceipt();
            }
            else
            {
                system( "cls" );
                cout<<"Wprowadzono niedozwolony znak";
            }
        }
        else
        {
            cout<<"\n\nChcesz zamowic cos jeszcze? T / N:";
            cin>>gotobeginning;
            if(gotobeginning=='T' || gotobeginning=='t')
            {
                goto starting;
                //return 0;
            }
            else if(gotobeginning=='N' || gotobeginning=='n')
            {
                system( "cls" );
                if(sum>=100)
                {
                    sum=sum*0.8;
                }
                file<<"\n\nDo zaplaty:  "<< sum <<"\n\n";
                file<<"\nZamowienie powyzej 100 zl - 20% znizki\n";
                file<<"Kierowca dostarczy twoje zamowienie w przeciagu 40 minut";
                file<<"\nDziekujemy i zapraszamy ponownie\n";

                file.close();
                readReceipt();
                printReceipt();
            }
            else
            {
                system( "cls" );
                cout<<"Wprowadzono niedozwolony znak";
            }
        }

    }
    else if(option==2)                            //Choose 50/50 pizza / pizzas
    {
        cout<<"\n1) "<<piz50_50_1<<"\n";
        cout<<"2) "<<piz50_50_2<<"\n";
        cout<<"3) "<<piz50_50_3<<"\n";
        cout<<"4) "<<piz50_50_4<<"\n";
        cout<<"5) "<<piz50_50_5<<"\n";
        cout<<"6) "<<piz50_50_6<<"\n\n";
        cout<<"7) "<<piz50_50_7<<"\n";
        cout<<"8) "<<piz50_50_8<<"\n";
        cout<<"9) "<<piz50_50_9<<"\n";
        cout<<"10) "<<piz50_50_10<<"\n";
        cout<<"11) "<<piz50_50_11<<"\n";
        cout<<"12) "<<piz50_50_12<<"\n";
        cout<<"0) Powrot do MENU";
        cout<<"\n\nTwoj wybor:";
        cin>>pizzaoption;
        if(pizzaoption>=1 && pizzaoption<=12)
        {
            cout<<"\n1) Mala (32 cm) \n"<<"2) Srednia (45 cm) \n"<<"3) Duza (60 cm)\n";      //Choose size
            cout<<"\n\nWybierz rozmiar:";
            cin>>pizzaoption1;
            if(pizzaoption1>=1 && pizzaoption1<=3)
            {
                cout<<"\nPodaj ilosc: ";
            }//Enter quantity
            else
            {
                system("cls");
                cout<<"Podano niewlasciwa wielkosc, Sprobuj ponownie\n";
                system("PAUSE");
                goto starting;
            }
            cin>>qty;
            count=count+qty;
            switch(pizzaoption1)
            {
            case 1: //Small
                if( pizzaoption == 1 || pizzaoption == 2 || pizzaoption == 4 || pizzaoption == 5 || pizzaoption == 7 || pizzaoption == 9 || pizzaoption == 11)
                {
                    option = 35*qty;
                }
                else if(pizzaoption == 8 || pizzaoption == 10 || pizzaoption == 12)
                {
                    option = 37*qty;
                }
                else if(pizzaoption == 3)
                {
                    option = 32*qty;
                }
                break;

            case 2: //Medium
                if( pizzaoption == 1 || pizzaoption == 2  || pizzaoption == 4  || pizzaoption == 5 || pizzaoption == 7 || pizzaoption == 9 || pizzaoption == 11)
                {
                    option = 45*qty;
                }
                else if(pizzaoption == 8 || pizzaoption == 10 || pizzaoption == 12)
                {
                    option = 48*qty;
                }
                else if(pizzaoption == 3)
                {
                    option = 42*qty;
                }
                break;

            case 3: //Large
                if( pizzaoption == 1 || pizzaoption == 2  || pizzaoption == 4  || pizzaoption == 5 || pizzaoption == 7 || pizzaoption == 9 || pizzaoption == 11)
                {
                    option = 60*qty;
                }
                else if(pizzaoption == 8 || pizzaoption == 10 || pizzaoption == 12)
                {
                    option = 65*qty;
                }
                else if(pizzaoption == 3)
                {
                    option = 56*qty;
                }
                break;

            }
            system("cls");                       //Console clear
            switch (pizzaoption)                //Chosen option display
            {
            case 1:
                cout<<"\t\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<piz50_50_1;
                cout<<"\n\nDo zaplaty:  "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<piz50_50_1;
                sum=sum+option;
                break;
            case 2:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<piz50_50_2;
                cout<<"\n\nDo zaplaty:  "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<piz50_50_2;
                sum=sum+option;
                break;
            case 3:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<piz50_50_3;
                cout<<"\n\nDo zaplaty:  "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<piz50_50_3;
                sum=sum+option;
                break;
            case 4:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<piz50_50_4;
                cout<<"\n\nDo zaplaty:  "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<piz50_50_4;
                sum=sum+option;
                break;
            case 5:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<piz50_50_5;
                cout<<"\n\nDo zaplaty:  "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<piz50_50_5;
                sum=sum+option;
                break;
            case 6:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<piz50_50_6;
                cout<<"\nDo zaplaty:  "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<piz50_50_6;
                sum=sum+option;
                break;
            case 7:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<piz50_50_7;
                cout<<"\n\nDo zaplaty:  "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<piz50_50_7;
                sum=sum+option;
                break;
            case 8:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<piz50_50_8;
                cout<<"\n\nDo zaplaty:  "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<piz50_50_8;
                sum=sum+option;
                break;
            case 9:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<piz50_50_9;
                cout<<"\n\nDo zaplaty:  "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<piz50_50_9;
                sum=sum+option;
                break;
            case 10:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<piz50_50_10;
                cout<<"\n\nDo zaplaty:  "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<piz50_50_10;
                sum=sum+option;
                break;
            case 11:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<piz50_50_11;
                cout<<"\n\nDo zaplaty:  "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<piz50_50_11;
                sum=sum+option;
                break;
            case 12:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<piz50_50_12;
                cout<<"\n\nDo zaplaty:  "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<piz50_50_12;
                sum=sum+option;
                break;


            }
            if(count >=2)
            {
                cout<<"\n1) "<<drink_cold_orange<<"\n";
                cout<<"2) "<<drink_cold_apple<<"\n";
                cout<<"3) "<<drink_cold_cherry<<"\n";
                cout<<"4) "<<drink_cold_mintapple<<"\n";
                cout<<"5) "<<drink_cold_applepeach<<"\n";
                cout<<"6) "<<drink_cold_cola<<"\n";
                cout<<"7) "<<drink_cold_pepsi<<"\n";
                cout<<"8) "<<drink_cold_fanta<<"\n";
                cout<<"9) "<<drink_cold_nesteacitrus<<"\n";
                cout<<"10) "<<drink_cold_nesteapeach<<"\n";
                cout<<"11) "<<drink_cold_water<<"\n";
                cout<<"\n\nWybierz swoj darmowy napoj: ";
                cin>>pizzaoption1;
                if(pizzaoption1>=1 && pizzaoption1<=11)
                {
                    switch(pizzaoption1)
                    {
                    case 1: option = 0.00;
                        break;

                    case 2: option = 0.00;
                        break;

                    case 3: option = 0.00;
                        break;

                    case 4: option = 0.00;
                        break;

                    case 5: option = 0.00;
                        break;

                    case 6: option = 0.00;
                        break;

                    case 7: option = 0.00;
                        break;

                    case 8: option = 0.00;
                        break;

                    case 9: option = 0.00;
                        break;

                    case 10: option = 0.00;
                        break;

                    case 11: option = 0.00;
                        break;
                    }
                    system("cls");
                    switch (pizzaoption1)             //Chosen promotion cold drink / drinks
                    {
                    case 1:
                        cout<<"\t\t ---------Wybrany napoj----------\n";
                        cout<<"\t\t\t"<<drink_cold_orange;
                        file<<"\n    "<<count/2<<" x "<<option<<"zl";
                        file<<"\t   "<<drink_cold_orange;
                        break;
                    case 2:
                        cout<<"\t\t ---------Wybrany napoj----------\n";
                        cout<<"\t\t\t"<<drink_cold_apple;
                        file<<"\n    "<<count/2<<" x "<<option<<"zl";
                        file<<"\t   "<<drink_cold_apple;
                        break;
                    case 3:
                        cout<<"\t\t ---------Wybrany napoj----------\n";
                        cout<<"\t\t\t"<<drink_cold_cherry;
                        file<<"\n    "<<count/2<<" x "<<option<<"zl";
                        file<<"\t   "<<drink_cold_cherry;
                        break;
                    case 4:
                        cout<<"\t\t ---------Wybrany napoj----------\n";
                        cout<<"\t\t\t"<<drink_cold_mintapple;
                        file<<"\n    "<<count/2<<" x "<<option<<"zl";
                        file<<"\t   "<<drink_cold_mintapple;
                        break;
                    case 5:
                        cout<<"\t\t ---------Wybrany napoj----------\n";
                        cout<<"\t\t\t"<<drink_cold_applepeach;
                        file<<"\n    "<<count/2<<" x "<<option<<"zl";
                        file<<"\t   "<<drink_cold_applepeach;
                        break;
                    case 6:
                        cout<<"\t\t ---------Wybrany napoj----------\n";
                        cout<<"\t\t\t"<<drink_cold_cola;
                        file<<"\n    "<<count/2<<" x "<<option<<"zl";
                        file<<"\t   "<<drink_cold_cola;
                        break;
                    case 7:
                        cout<<"\t\t ---------Wybrany napoj----------\n";
                        cout<<"\t\t\t"<<drink_cold_pepsi;
                        file<<"\n    "<<count/2<<" x "<<option<<"zl";
                        file<<"\t   "<<drink_cold_pepsi;
                        break;
                    case 8:
                        cout<<"\t\t ---------Wybrany napoj----------\n";
                        cout<<"\t\t\t"<<drink_cold_fanta;
                        file<<"\n    "<<count/2<<" x "<<option<<"zl";
                        file<<"\t   "<<drink_cold_fanta;
                        break;
                    case 9:
                        cout<<"\t\t ---------Wybrany napoj----------\n";
                        cout<<"\t\t\t"<<drink_cold_nesteacitrus;
                        file<<"\n    "<<count/2<<" x "<<option<<"zl";
                        file<<"\t   "<<drink_cold_nesteacitrus;
                        break;
                    case 10:
                        cout<<"\t\t ---------Wybrany napoj----------\n";
                        cout<<"\t\t\t"<<drink_cold_nesteapeach;
                        file<<"\n    "<<count/2<<" x "<<option<<"zl";
                        file<<"\t   "<<drink_cold_nesteapeach;
                        break;
                    case 11:
                        cout<<"\t\t ---------Wybrany napoj----------\n";
                        cout<<"\t\t\t"<<drink_cold_water;
                        file<<"\n    "<<count/2<<" x "<<option<<"zl";
                        file<<"\t   "<<drink_cold_water;
                        break;

                    }

                }
                cout<<"\n\nChcialbys dodac cos do zamowienia? T / N:";
                cin>>gotobeginning;
                if(gotobeginning=='T' || gotobeginning=='t')
                {
                    goto starting;
                    //return 0;
                }
                else if(gotobeginning=='N' || gotobeginning=='n')
                {
                    system( "cls" );
                    if(sum>=100)
                    {
                        sum=sum*0.8;
                    }
                    file<<"\n\nDo zaplaty:  "<< sum <<"\n\n";
                    file<<"\nZamowienie powyzej 100 zl - 20% znizki\n";
                    file<<"Kierowca dostarczy twoje zamowienie w przeciagu 40 minut";
                    file<<"\nDziekujemy i zapraszamy ponownie\n";

                    file.close();
                    readReceipt();
                    printReceipt();
                }
                else
                {
                    system( "cls" );
                    cout<<"Wprowadzono niedozwolony znak";
                }

            }
            else
            {
                cout<<"\n\nChcialbys dodac cos do zamowienia? T / N:";
                cin>>gotobeginning;
                if(gotobeginning=='T' || gotobeginning=='t')
                {
                    goto starting;
                    //return 0;
                }
                else if(gotobeginning=='N' || gotobeginning=='n')
                {
                    system( "cls" );
                    if(sum>=100)
                    {
                        sum=sum*0.8;
                    }
                    file<<"\n\nDo zaplaty:  "<< sum <<"\n\n";
                    file<<"\nZamowienie powyzej 100 zl - 20% znizki\n";
                    file<<"Kierowca dostarczy twoje zamowienie w przeciagu 40 minut";
                    file<<"\nDziekujemy i zapraszamy ponownie\n";

                    file.close();
                    readReceipt();
                    printReceipt();
                }
                else
                {
                    system( "cls" );
                    cout<<"Wprowadzono niedozwolony znak";
                }
            }
        }
        else if(pizzaoption == 0)
        {
            system("cls");
            goto starting;
        }
        else
        {
            system("cls");
            cout<<"Wprowadzono niedozwolony znak, sprobuj ponownie\n";
            system("PAUSE");
            goto starting;
        }
    }
    else if(option==3)                            //Choose set
    {
        cout<<"\n1) "<<set_1<<"\n";
        cout<<"2) "<<set_2<<"\n";
        cout<<"3) "<<set_3<<"\n";
        cout<<"4) "<<set_4<<"\n";
        cout<<"0) Powrot do MENU";
        cout<<"\n\nTwoj wybor:";
        cin>>pizzaoption1;
        if(pizzaoption1>=1 && pizzaoption1<=4)
        {
            cout<<"\n\nPodaj ilosc: ";            //Enter quantity
            cin>>qty;
            switch(pizzaoption1)
            {
            case 1: option = 85.00*qty;
                break;

            case 2: option = 30.00*qty;
                break;

            case 3: option = 120.00*qty;
                break;

            case 4: option = 50.00*qty;
                break;

            }
            system("cls");
            switch (pizzaoption1)              //Chosen option
            {
            case 1:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<set_1;
                cout<<"\n\nDo zaplaty: "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<set_1;
                sum=sum+option;
                break;
            case 2:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<"  "<<set_2;
                cout<<"\n\nDo zaplaty: "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<set_2;
                sum=sum+option;
                break;
            case 3:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<set_3;
                cout<<"\n\nDo zaplaty: "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<set_3;
                sum=sum+option;
                break;
            case 4:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<set_4;
                cout<<"\n\nDo zaplaty: "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<set_4;
                sum=sum+option;
                break;


            }
            cout<<"\n\nChcialbys dodac cos do zamowienia? T / N:";
            cin>>gotobeginning;
            if(gotobeginning=='T' || gotobeginning=='t')
            {
                goto starting;
                //return 0;
            }
            else if(gotobeginning=='N' || gotobeginning=='n')
            {
                system( "cls" );
                if(sum>=100)
                {
                    sum=sum*0.8;
                }
                file<<"\n\nDo zaplaty:  "<< sum <<"\n\n";
                file<<"\nZamowienie powyzej 100 zl - 20% znizki\n";
                file<<"Kierowca dostarczy twoje zamowienie w przeciagu 40 minut";
                file<<"\nDziekujemy i zapraszamy ponownie\n";

                file.close();
                readReceipt();
                printReceipt();

            }
        }
        else if(pizzaoption1 == 0)
        {
            goto starting;
        }
        else
        {
            system("cls");
            cout<<"Wprowadzono niedozwolony znak, sprobuj ponownie\n";
            system("PAUSE");
            goto starting;
        }
    }
    else if(option==4)                              //Choose cold drink / drinks
    {
        cout<<"\n1) "<<drink_cold_orange<<"\n";
        cout<<"2) "<<drink_cold_apple<<"\n";
        cout<<"3) "<<drink_cold_cherry<<"\n";
        cout<<"4) "<<drink_cold_mintapple<<"\n";
        cout<<"5) "<<drink_cold_applepeach<<"\n";
        cout<<"6) "<<drink_cold_cola<<"\n";
        cout<<"7) "<<drink_cold_pepsi<<"\n";
        cout<<"8) "<<drink_cold_fanta<<"\n";
        cout<<"9) "<<drink_cold_nesteacitrus<<"\n";
        cout<<"10) "<<drink_cold_nesteapeach<<"\n";
        cout<<"11) "<<drink_cold_water<<"\n";
        cout<<"0) Powrot do MENU";
        cout<<"\n\nPodaj ktory napoj chesz: ";
        cin>>pizzaoption1;
        if(pizzaoption1>=1 && pizzaoption1<=11)
        {
            cout<<"\n\nPodaj ilosc: ";              //Enter quantity
            cin>>qty;
            switch(pizzaoption1)
            {
            case 1: option = 3.50*qty;
                break;

            case 2: option = 3.50*qty;
                break;

            case 3: option = 3.50*qty;
                break;

            case 4: option = 3.50*qty;
                break;

            case 5: option = 3.50*qty;
                break;

            case 6: option = 7.00*qty;
                break;

            case 7: option = 7.00*qty;
                break;

            case 8: option = 7.00*qty;
                break;

            case 9: option = 4.50*qty;
                break;

            case 10: option = 4.50*qty;
                break;

            case 11: option = 3.00*qty;
                break;

            }
            system("cls");
            switch (pizzaoption1)             //Chosen cold drink / drinks
            {
            case 1:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<drink_cold_orange;
                cout<<"\n\nDo zaplaty: "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<drink_cold_orange;
                sum=sum+option;
                break;
            case 2:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<"  "<<drink_cold_apple;
                cout<<"\n\nDo zaplaty: "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<drink_cold_apple;
                sum=sum+option;
                break;
            case 3:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<drink_cold_cherry;
                cout<<"\n\nDo zaplaty: "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<drink_cold_cherry;
                sum=sum+option;
                break;
            case 4:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<drink_cold_mintapple;
                cout<<"\n\nDo zaplaty: "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<drink_cold_mintapple;
                sum=sum+option;
                break;
            case 5:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<drink_cold_applepeach;
                cout<<"\n\nDo zaplaty: "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<drink_cold_applepeach;
                sum=sum+option;
                break;
            case 6:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<drink_cold_cola;
                cout<<"\n\nDo zaplaty: "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<drink_cold_cola;
                sum=sum+option;
                break;
            case 7:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<drink_cold_pepsi;
                cout<<"\n\nDo zaplaty: "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<drink_cold_pepsi;
                sum=sum+option;
                break;
            case 8:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<drink_cold_fanta;
                cout<<"\n\nDo zaplaty: "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<drink_cold_fanta;
                sum=sum+option;
                break;
            case 9:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<drink_cold_nesteacitrus;
                cout<<"\n\nDo zaplaty: "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<drink_cold_nesteacitrus;
                sum=sum+option;
                break;
            case 10:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<drink_cold_nesteapeach;
                cout<<"\n\nDo zaplaty: "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<drink_cold_nesteapeach;
                sum=sum+option;
                break;
            case 11:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<drink_cold_water;
                cout<<"\n\nDo zaplaty: "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<drink_cold_water;
                sum=sum+option;
                break;

            }
            cout<<"\n\nChcialbys dodac cos do zamowienia? T / N:";
            cin>>gotobeginning;
            if(gotobeginning=='T' || gotobeginning=='t')
            {
                goto starting;
                //return 0;
            }
            else if(gotobeginning=='N' || gotobeginning=='n')
            {
                system( "cls" );
                if(sum>=100)
                {
                    sum=sum*0.8;
                }
                file<<"\n\nDo zaplaty:  "<< sum <<"\n\n";
                file<<"\nZamowienie powyzej 100 zl - 20% znizki\n";
                file<<"Kierowca dostarczy twoje zamowienie w przeciagu 40 minut";
                file<<"\nDziekujemy i zapraszamy ponownie\n";

                file.close();
                readReceipt();
                printReceipt();

            }
        }
        else if(pizzaoption1 == 0)
        {
            system( "cls" );
            goto starting;
        }
        else
        {
            system("cls");
            cout<<"Wprowadzono niedozwolony znak, sprobuj ponownie\n";
            system("PAUSE");
            goto starting;
        }
    }
    else if(option==5)                            //Choose hot drink / drinks
    {
        cout<<"\n1) "<<drink_hot_latte<<"\n";
        cout<<"2) "<<drink_hot_cappucino<<"\n";
        cout<<"3) "<<drink_hot_espresso<<"\n";
        cout<<"4) "<<drink_hot_blacktea<<"\n";
        cout<<"5) "<<drink_hot_greentea<<"\n";
        cout<<"0) Powrot do MENU";
        cout<<"\n\nTwoj wybor: ";
        cin>>pizzaoption1;
        if(pizzaoption1>=1 && pizzaoption1<=5)
        {
            cout<<"\n\nPodaj ilosc: ";             //Choose quantity
            cin>>qty;
            switch(pizzaoption1)
            {
            case 1: option = 14.00*qty;
                break;

            case 2: option = 12.00*qty;
                break;

            case 3: option = 10.00*qty;
                break;

            case 4: option = 6.00*qty;
                break;

            case 5: option = 6.00*qty;
                break;

            }
            system("cls");
            switch (pizzaoption1)                      //Chosen hot drink / drinks
            {
            case 1:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<drink_hot_latte;
                cout<<"\n\nDo zaplaty: "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<drink_hot_latte;
                sum=sum+option;
                break;
            case 2:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<"  "<<drink_hot_cappucino;
                cout<<"\n\nDo zaplaty: "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<drink_hot_cappucino;
                sum=sum+option;
                break;
            case 3:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<drink_hot_espresso;
                cout<<"\n\nDo zaplaty: "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<drink_hot_espresso;
                sum=sum+option;
                break;
            case 4:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<drink_hot_blacktea;
                cout<<"\n\nDo zaplaty: "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<drink_hot_blacktea;
                sum=sum+option;
                break;
            case 5:
                cout<<"\t\t--------Twoje zamowienie---------\n";
                cout<<"\t\t      "<<qty<<" x "<<drink_hot_greentea;
                cout<<"\n\nDo zaplaty: "<<option<<"zl";
                file<<"\n    "<<qty<<" x "<<option/qty<<"zl";
                file<<"\t   "<<drink_hot_greentea;
                sum=sum+option;
                break;


            }
            cout<<"\n\nChcialbys dodac cos do zamowienia? T / N:";
            cin>>gotobeginning;
            if(gotobeginning=='T' || gotobeginning=='t')
            {
                goto starting;
                //return 0;

            }
            else if(gotobeginning=='N' || gotobeginning=='n')
            {
                system( "cls" );
                if(sum>=100)                           //20% OFF promotion if sum > 100
                {
                    sum=sum*0.8;
                }
                file<<"\n\nDo zaplaty:  "<< sum <<"\n\n";
                file<<"\nZamowienie powyzej 100 zl - 20% znizki\n";
                file<<"Kierowca dostarczy twoje zamowienie w przeciagu 40 minut";
                file<<"\nDziekujemy i zapraszamy ponownie\n";

                file.close();
                readReceipt();
                printReceipt();

            }
        }
        else if(pizzaoption1 == 0)
        {
            system( "cls" );             //Clear console
            goto starting;
        }
        else
        {
            system("cls");
            cout<<"Wprowadzono niedozwolony znak, sprobuj ponownie\n";
            system("PAUSE");
            goto starting;
        }
    }
    else
    {
        system("cls");
        cout<<"Podano niewlasciwy znak, sprobuj ponownie\n";
        system("PAUSE");
        goto starting;
    }
};
