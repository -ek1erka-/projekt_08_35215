#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>
#include <string>
#include <windows.h>
#include <fstream>

using namespace std;

//================================================

class cPizzeria
{
public:
    cPizzeria();
    void printMenu();
    void printReceipt();
    void colorTxt(string Txt, int color);

private:
    vector<string> vMenu;
    vector<string> vReceipt;

    void readMenu();

protected:
    void readReceipt();
};

//================================================

class cData
{
public:
    //Pizza
           //Red pizzas
    string piz1,
           piz2,
           piz3,
           piz4,
           //White pizzas
           piz5,
           piz6,
           piz7,
           piz8;

    // 50/50 pizzas
    string piz50_50_1,
           piz50_50_2,
           piz50_50_3,
           piz50_50_4,
           piz50_50_5,
           piz50_50_6,
           piz50_50_7,
           piz50_50_8,
           piz50_50_9,
           piz50_50_10,
           piz50_50_11,
           piz50_50_12;

    //Sets
    string set_1,
           set_2,
           set_3,
           set_4;

    //Drinks
           //Cold
    string drink_cold_orange,
           drink_cold_apple,
           drink_cold_cherry,
           drink_cold_mintapple,
           drink_cold_applepeach,
           drink_cold_cola,
           drink_cold_pepsi,
           drink_cold_fanta,
           drink_cold_nesteacitrus,
           drink_cold_nesteapeach,
           drink_cold_water,
           //Hot
           drink_hot_latte,
           drink_hot_cappucino,
           drink_hot_espresso,
           drink_hot_blacktea,
           drink_hot_greentea;


    cData();             //Constructor
    void vReceipt();
};

//================================================

class cOrder:public cData, public cPizzeria
{
public:
    int option=0,
        pizzaoption,
        pizzaoption1,
        promotion,
        qty,             //amount of products for 20% off promotion
        count=0;         //register for 2 pizzas + 1 free drink promotion
    char gotobeginning;
    float sum;           //total sum

    cOrder();
    void Order();

};

//================================================

#endif // PIZZERIA_H
